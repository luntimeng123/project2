package Mudul;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {

       public Connection getConnection(){

                Connection connection = null;
           try {

               Class.forName("org.postgresql.Driver");

               //connection to database
               connection = DriverManager.getConnection(
                       "jdbc:postgresql://localhost:5432/StockManagement",
                       "postgres",
                       "timeng123#"
               );

               //System.out.println("success.!");
           } catch (ClassNotFoundException | SQLException e) {
               e.printStackTrace();
           }

           return connection;
       }

}
