package Project;

import java.util.ArrayList;
import java.util.List;

public class ManageText extends Thread{
    //Logo place
    public void logo(){
        try{
            List<String> arr = new ArrayList<>();
            arr.add("  _____  _                           _____           _                 _____ ____  ");
            arr.add(" |  __ \\| |                         |  __ \\         | |               / ____|___ \\ ");
            arr.add(" | |__) | |__  _ __   ___  _ __ ___ | |__) |__ _ __ | |__    ______  | |  __  __) |");
            arr.add(" |  ___/| '_ \\| '_ \\ / _ \\| '_ ` _ \\|  ___/ _ \\ '_ \\| '_ \\  |______| | | |_ ||__ < ");
            arr.add(" | |    | | | | | | | (_) | | | | | | |  |  __/ | | | | | |          | |__| |___) |");
            arr.add(" |_|    |_| |_|_| |_|\\___/|_| |_| |_|_|   \\___|_| |_|_| |_|           \\_____|____/ ");
            arr.add("                                                                                   ");
            for (int i=0;i<arr.size();i++){
                System.out.println(arr.get(i));
                Thread.sleep(200);
            }
        }catch(Exception e){
            System.out.println(e);
        }
    }

    public void loading(List<Products> arr){
        String text = " Please Wait";
        String time = " Spent time: ";
        String dot = "......";
        int mili = arr.size();
        try {
            System.out.print(text);
            for (int i=0;i<dot.length();i++){
                System.out.print(dot.charAt(i));
                Thread.sleep(mili/6);
            }
            System.out.println("\n"+time+(mili/1000)+" s");
            System.out.println("\n");
        }catch (Exception e){
            System.out.println(e);
        }
    }

}
