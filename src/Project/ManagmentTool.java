package Project;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class ManagmentTool {


    //insert data to database
    public void insertDataToDatabase(Products products, Connection connection) {

        String strWrite = "insert into product(id,pname,unit_price,qty,date) VALUES(" +
                ""+ products.getId()+",'"
                + products.getPname()+"',"
                + products.getUnit_price()+","
                + products.getQty()+",'"
                + products.getDate()+"')";

        try {

            PreparedStatement statement = connection.prepareStatement(strWrite);
            statement.executeUpdate();
//            if (EU>0){
//                System.out.println("Success Insert-->");
//            }else {
//                System.out.println("Fail Insert--> ");
//            }

            statement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        //end method
    }

    //delete data from database
    public void deleteDataFromDatabase(Connection connection,int id){
        String strDelete = "delete from product where id="+id;

        try {

            PreparedStatement statement = connection.prepareStatement(strDelete);
            statement.executeUpdate();

//            if (EU>0){
//                System.out.println("Success Delete-->");
//            }else {
//                System.out.println("Fail Delete--> ");
//            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //update data to database
    public void updateDataToDatabase(Connection connection,Products products){

        String strUpdate = "UPDATE product set pname=?,unit_price=?,qty=?,date=? where id= ?";


        try {
            PreparedStatement statement = connection.prepareStatement(strUpdate);

            statement.setString(1,products.getPname());
            statement.setFloat(2,products.getUnit_price());
            statement.setInt(3,products.getQty());
            statement.setString(4,products.getDate());
            statement.setInt(5,products.getId());

            statement.executeUpdate();

//            if (EU>0){
//                System.out.println("Success Update-->");
//            }else {
//                System.out.println("Fail Update--> ");
//            }

            statement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //display data
    public  void RestoreProduck(Connection connection, Products products,List<Products> arr) {

        try {

            String sql = "Select * from product";
            Statement st = connection.createStatement();

            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {

                products = new Products(rs.getInt(1), rs.getString(2),
                        rs.getFloat(3), rs.getInt(4), rs.getString(5));

                //System.out.println(products.toString());
                arr.add(products);

                //sorting ------------------
                arr.sort(Comparator.comparing(Products::getId));

            }

            System.out.println(" -----------! Success Resotre !-----------");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    //end database method-----------------------------------------------------------------------------------------------



    //display data in ArrayList
    public int displayPage(List<Products> arr,int page,int row){
        List<Integer> numk = new ArrayList<>();//store num
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t.addCell(" ID ",numberStyle);
        t.addCell(" Product-Name ",numberStyle);
        t.addCell(" Uint-Price ",numberStyle);
        t.addCell(" QTY ",numberStyle);
        t.addCell(" Date ",numberStyle);
        int j=0;
        for (int i=-1;i<arr.size();i+=(row-1)){
            numk.add(i);
        }
        if (page == 0){
            page =(arr.size()/row);
            if ((arr.size()%row)==0){
                int k = page + numk.get(page-1);
                for (int i=k;i<arr.size();i++){
                    if (j==row){
                        t.addCell("Page "+page+" /"+(arr.size()/row),numberStyle,2);
                        t.addCell("Record: "+arr.size(),numberStyle,3);
                        break;
                    }else {
                        t.addCell(" "+arr.get(i).getId()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getPname()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getUnit_price()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getQty()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getDate()+" ",numberStyle);
                        j = j+1;
                    }
                }
                //check last page
                if (page == (arr.size()/row)){
                    t.addCell("Page "+page+" /"+(arr.size()/row),numberStyle,2);
                    t.addCell("Record: "+arr.size(),numberStyle,3);
                }
            }else {
                page=page+1;
                int k = page + numk.get(page-1);
                for (int i=k;i<arr.size();i++){
                    if (j==row){
                        t.addCell("Page "+page+" /"+((arr.size()/row)+1),numberStyle,2);
                        t.addCell("Record: "+arr.size(),numberStyle,3);
                        break;
                    }else {
                        t.addCell(" "+arr.get(i).getId()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getPname()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getUnit_price()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getQty()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getDate()+" ",numberStyle);
                        j = j+1;
                    }
                }
                //check last page
                System.out.println(page);
                if (page == ((arr.size()/row)+1)){
                    t.addCell("Page "+page+" /"+((arr.size()/row)+1),numberStyle,2);
                    t.addCell("Record: "+arr.size(),numberStyle,3);
                }
            }
        } else {
            if ((arr.size()%row)==0){
                if ((arr.size()/row)>= page){
                    int k = page + numk.get(page-1);
                    for (int i=k;i<arr.size();i++){
                        if (j==row){
                            t.addCell("Page "+page+" /"+(arr.size()/row),numberStyle,2);
                            t.addCell("Record: "+arr.size(),numberStyle,3);
                            break;
                        }else {
                            t.addCell(" "+arr.get(i).getId()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getPname()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getUnit_price()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getQty()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getDate()+" ",numberStyle);
                            j = j+1;
                        }
                    }
                }else {
                    System.out.println("out of page range.!");
                }
                //check last page
                if (page ==(arr.size()/row) ){
                    t.addCell("Page "+page+" /"+(arr.size()/row),numberStyle,2);
                    t.addCell("Record: "+arr.size(),numberStyle,3);
                }
            }//---------------------------------------------------------------------------------------------------------
            else {
                int nsize = (arr.size()/row)+1;
                if (nsize>=page){
                    int k = page + numk.get(page-1);
                    for (int i=k;i<arr.size();i++){
                        if (j==row){
                            t.addCell("Page "+page+" /"+nsize,numberStyle,2);
                            t.addCell("Record: "+arr.size(),numberStyle,3);
                            break;
                        }else {
                            t.addCell(" "+arr.get(i).getId()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getPname()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getUnit_price()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getQty()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getDate()+" ",numberStyle);
                            j = j+1;
                        }
                    }
                }else {
                    System.out.println("out of page range.!");
                }
                //check last page
                if (page == nsize){
                    t.addCell("Page "+page+" /"+nsize,numberStyle,2);
                    t.addCell("Record: "+arr.size(),numberStyle,3);
                }
            }
        }
        System.out.println(t.render());
        return page;
    }

    //write data to ArrayList
    public int writeDataToArray(List<Products> arr,int autoNumber,Products products,Connection connection){

        Validation val = new Validation();
        Scanner sc = new Scanner(System.in);

        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t1 = new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        t1.addCell("Record was Add to Database -->",numberStyle);

        LocalDateTime datetime1 = LocalDateTime.now();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formatDateTime = datetime1.format(format);

        System.out.print(" " + "-->> Product_id: " + autoNumber + "\n");
        System.out.print(" " + "-->> Product_name: ");
        String strname = sc.nextLine();
        System.out.print(" " + "-->> Product_price: ");
        String strprice = sc.nextLine();
        float price = Float.parseFloat(strprice);
        System.out.print(" " + "-->> Product_qty: ");
        String strqty = sc.nextLine();
        int qty = Integer.parseInt(strqty);

        products = new Products(autoNumber,strname,price,qty,formatDateTime);

        arr.add(products);

        System.out.print("-->> Do you want to Add data to Database y/Y-(Yes) or n/N-(No): ");
        String writeData = sc.nextLine().toLowerCase();
        boolean check = val.checkStr(writeData);
        while (!check){
            System.out.print("-->> Do you want to Add data to Database y/Y-(Yes) or n/N-(No): ");
            writeData = sc.nextLine().toLowerCase();
            check = val.checkStr(writeData);
        }
            switch (writeData){

                case "y":

                    insertDataToDatabase(products,connection);
                    System.out.println(t1.render());

                    break;

                case "n":
                    break;

                default:
                    System.out.println("----------! You Choose Wrong Option !----------");
            }

        int lastId = arr.get(arr.size()-1).getId();

        return lastId;
    }

    //Read data from ArrayList by Product_id
    public void readDataById(List<Products> arr,int product_id){

        int countRow = 0;

        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        Table t1 = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.SURROUND);

        t.addCell("ID ",numberStyle);
        t.addCell("Product-Name ",numberStyle);
        t.addCell("Uint-Price ",numberStyle);
        t.addCell("QTY ",numberStyle);
        t.addCell("Date ",numberStyle);
        t1.addCell("-----------! No Id Match !-----------",numberStyle);

        for (int i=0;i<arr.size();i++){
            if (arr.get(i).getId() == product_id){

                t.addCell(" "+arr.get(i).getId()+" ",numberStyle);
                t.addCell(" "+arr.get(i).getPname()+" ",numberStyle);
                t.addCell(" "+arr.get(i).getUnit_price()+" ",numberStyle);
                t.addCell(" "+arr.get(i).getQty()+" ",numberStyle);
                t.addCell(" "+arr.get(i).getDate()+" ",numberStyle);

                System.out.println(t.render());

            }else {

                countRow = countRow +1;

                if (countRow == arr.size()){

                    System.out.println(t1.render());

                }else {

                    continue;

                }
            }
        }

    }

    //Update data to ArrayList
    public List<Products> updateDateToArrayList(List<Products> arr,int product_id,Products products,Connection connection){

        Validation val = new Validation();
        Scanner sc = new Scanner(System.in);

        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.SURROUND);
        Table t1 = new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        Table t2 = new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        Table t3= new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        Table t4= new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        Table t5= new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);

        readDataById(arr,product_id);

        int product_index = product_id-1;

        t.addCell("1)name "+" | ",numberStyle);
        t.addCell("2)price "+" | ",numberStyle);
        t.addCell("3)qty "+" | ",numberStyle);
        t.addCell("4)All "+" | ",numberStyle);
        t.addCell("5)menu "+" | ",numberStyle);
        System.out.println(t.render());

        t1.addCell("<----------- Update Name Success Only In Table ----------->",numberStyle);
        t2.addCell("<----------- Update Data Success To Database ----------->",numberStyle);
        t3.addCell("<----------- Update Unit-Price Success Only in Table ----------->",numberStyle);
        t4.addCell("<----------- Update Qty Success Only in Table ----------->",numberStyle);
        t5.addCell("<----------- Update All Success Only in Table ----------->",numberStyle);

        System.out.print("-->> Choose update option: ");
        String udatoption = sc.nextLine();
        boolean check = val.checkNum(udatoption);
        while (!check){
            System.out.println("Enter only number: ");
            udatoption = sc.nextLine();
            check = val.checkNum(udatoption);
        }
        int intUpdateOption = Integer.parseInt(udatoption);

        while (intUpdateOption !=5){

            switch (intUpdateOption){

                case 1:
                    System.out.println("----------Update Name----------");

                    LocalDateTime datetime1 = LocalDateTime.now();
                    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    String formatDateTime = datetime1.format(format);

                    System.out.print("Enter Product_Name: ");
                    String name = sc.nextLine();
                    check = val.checkStr(name);
                    while (!check){
                        System.out.print("Enter Product_Name again: ");
                        name = sc.nextLine();
                        check = val.checkStr(name);
                    }

                    arr.get(product_index).setPname(name);
                    arr.get(product_index).setDate(formatDateTime);

                    readDataById(arr,product_id);

                    System.out.print("-->> Do you want to Update to Database y/Y-(Yes) or n/N-(NO): ");
                    String updateChoiceToBatabase = sc.nextLine().toLowerCase();
                    check = val.checkStr(updateChoiceToBatabase);
                    while (!check){
                            System.out.print("Enter Choice again y/Y-(Yes) or n/N-(NO): ");
                            updateChoiceToBatabase = sc.nextLine().toLowerCase();
                            check = val.checkStr(updateChoiceToBatabase);
                    }

                    switch (updateChoiceToBatabase){
                            case "y":

                                    //update to class
                                    products = new Products(product_id,
                                    arr.get(product_index).getPname(),
                                    arr.get(product_index).getUnit_price(),
                                    arr.get(product_index).getQty(),
                                    arr.get(product_index).getDate());

                                    updateDataToDatabase(connection,products);//update to database
                                    System.out.println(t2.render());
                                    intUpdateOption = 5;
                            break;

                            case "n":
                                    System.out.println(t1.render());
                                    intUpdateOption = 5;
                            break;

                            default:
                                    System.out.println(t1.render());
                                    intUpdateOption = 5;
                            }

                    break;

                case 2:
                    System.out.println("----------Update Price----------");

                    datetime1 = LocalDateTime.now();
                    format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    formatDateTime = datetime1.format(format);

                    System.out.print("Enter Product_Price: ");
                    String price = sc.nextLine();
                    float fprice = Float.parseFloat(price);

                    arr.get(product_index).setUnit_price(fprice);
                    arr.get(product_index).setDate(formatDateTime);

                    readDataById(arr,product_id);

                    System.out.print("-->> Do you want to Update to Database y/Y-(Yes) or n/N-(NO): ");
                    updateChoiceToBatabase = sc.nextLine().toLowerCase();
                    check = val.checkStr(updateChoiceToBatabase);
                    while (!check){
                        System.out.print("Enter Choice again y/Y-(Yes) or n/N-(NO): ");
                        updateChoiceToBatabase = sc.nextLine().toLowerCase();
                        check = val.checkStr(updateChoiceToBatabase);
                    }

                    switch (updateChoiceToBatabase){
                        case "y":

                            //update to class
                            products = new Products(product_id,
                                    arr.get(product_index).getPname(),
                                    arr.get(product_index).getUnit_price(),
                                    arr.get(product_index).getQty(),
                                    arr.get(product_index).getDate());

                            updateDataToDatabase(connection,products);//update to database
                            System.out.println(t2.render());
                            intUpdateOption = 5;
                            break;

                        case "n":
                            System.out.println(t3.render());
                            intUpdateOption = 5;
                            break;

                        default:
                            System.out.println(t3.render());
                            intUpdateOption = 5;
                    }

                    break;
                case 3:
                    System.out.println("----------Update Quantity----------");

                    datetime1 = LocalDateTime.now();
                    format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    formatDateTime = datetime1.format(format);

                    System.out.print("Enter Product_Price: ");
                    String qty = sc.nextLine();
                    check = val.checkNum(qty);
                    while (!check){
                        System.out.print("Enter Product_Price: ");
                        qty = sc.nextLine();
                        check = val.checkNum(qty);
                    }
                    int intqty = Integer.parseInt(qty);

                    arr.get(product_index).setQty(intqty);
                    arr.get(product_index).setDate(formatDateTime);

                    readDataById(arr,product_id);

                    System.out.print("-->> Do you want to Update to Database y/Y-(Yes) or n/N-(NO): ");
                    updateChoiceToBatabase = sc.nextLine().toLowerCase();
                    check = val.checkStr(updateChoiceToBatabase);
                    while (!check){
                        System.out.print("Enter Choice again y/Y-(Yes) or n/N-(NO): ");
                        updateChoiceToBatabase = sc.nextLine().toLowerCase();
                        check = val.checkStr(updateChoiceToBatabase);
                    }

                    switch (updateChoiceToBatabase){
                        case "y":

                            //update to class
                            products = new Products(product_id,
                                    arr.get(product_index).getPname(),
                                    arr.get(product_index).getUnit_price(),
                                    arr.get(product_index).getQty(),
                                    arr.get(product_index).getDate());

                            updateDataToDatabase(connection,products);//update to database
                            System.out.println(t2.render());
                            intUpdateOption = 5;
                            break;

                        case "n":
                            System.out.println(t4.render());
                            intUpdateOption = 5;
                            break;

                        default:
                            System.out.println(t4.render());
                            intUpdateOption = 5;
                    }

                    break;
                case 4:
                    System.out.println("----------Update All----------");

                    datetime1 = LocalDateTime.now();
                    format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    formatDateTime = datetime1.format(format);

                    System.out.print("Enter Product_Name: ");
                    name = sc.nextLine();
                    check = val.checkStr(name);
                    while (!check){
                        System.out.print("Enter Product_Name again: ");
                        name = sc.nextLine();
                        check = val.checkStr(name);
                    }
                    System.out.print("Enter Product_Price: ");
                    price = sc.nextLine();
                    fprice = Float.parseFloat(price);
                    System.out.print("Enter Product_Price: ");
                    qty = sc.nextLine();
                    check = val.checkNum(qty);
                    while (!check){
                        System.out.print("Enter Product_Price: ");
                        qty = sc.nextLine();
                        check = val.checkNum(qty);
                    }
                    intqty = Integer.parseInt(qty);

                    arr.get(product_index).setPname(name);
                    arr.get(product_index).setUnit_price(fprice);
                    arr.get(product_index).setQty(intqty);
                    arr.get(product_index).setDate(formatDateTime);

                    System.out.print("-->> Do you want to Update to Database y/Y-(Yes) or n/N-(NO): ");
                    updateChoiceToBatabase = sc.nextLine().toLowerCase();
                    check = val.checkStr(updateChoiceToBatabase);
                    while (!check){
                        System.out.print("Enter Choice again y/Y-(Yes) or n/N-(NO): ");
                        updateChoiceToBatabase = sc.nextLine().toLowerCase();
                        check = val.checkStr(updateChoiceToBatabase);
                    }

                    switch (updateChoiceToBatabase){
                        case "y":

                            //update to class
                            products = new Products(product_id,
                                    arr.get(product_index).getPname(),
                                    arr.get(product_index).getUnit_price(),
                                    arr.get(product_index).getQty(),
                                    arr.get(product_index).getDate());

                            updateDataToDatabase(connection,products);//update to database
                            System.out.println(t2.render());
                            intUpdateOption = 5;
                            break;

                        case "n":
                            System.out.println(t5.render());
                            intUpdateOption = 5;
                            break;

                        default:
                            System.out.println(t5.render());
                            intUpdateOption = 5;
                    }

                    break;
                case 5:

                    intUpdateOption = 5;

                    break;

                default:
                    System.out.println("------------! You Choose Wrong Option '_' !------------");
                    intUpdateOption = 5;

            }//end switch-----
        }//end while-----
        return arr;
    }

    //Remove data From ArrayList
    public int removeDataFromArrayList(List<Products> arr,int product_id,Connection connection){

        int k=0;

        Validation val = new Validation();
        Scanner sc= new Scanner(System.in);
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t1 = new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        Table t2 = new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        Table t3 = new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        Table t4 = new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        t1.addCell("----------! id "+product_id+" not include in system. !----------",numberStyle);
        t2.addCell("<---------- Record was Removed from Database ---------->",numberStyle);
        t3.addCell("<---------- Data Removed From Table But Not Database ---------->",numberStyle);

        readDataById(arr,product_id);

        for (int i=0;i<arr.size();i++){
            if (arr.get(i).getId() == product_id){

                arr.remove(arr.get(i));

                t4.addCell("<---------- Success Remove "+"Product "+product_id+" From Table ---------->",numberStyle);
                System.out.println(t4.render());

                System.out.print("Do you want to Delete form Database y/Y-(Yes) or n/N-(No): ");
                String deleteData = sc.nextLine();
                Boolean check = val.checkStr(deleteData);
                while (!check){
                    System.out.print("Do you want to Delete form Database y/Y-(Yes) or n/N-(No): ");
                    deleteData = sc.nextLine();
                    check = val.checkStr(deleteData);
                }

                switch (deleteData){

                    case "y":

                        deleteDataFromDatabase(connection,product_id);
                        System.out.println(t2.render());

                        break;

                    case "n":

                        System.out.println(t3.render());

                        break;

                    default:
                        System.out.println("----------! You Choose Wrong Option !----------");
                }

            }else {

                k = k+1;

                if (k == arr.size()){

                    System.out.println(t1.render());
                }else {
                    continue;
                }

            }
        }

        return arr.size();
    }

    //searchByName
    public void searchByName(){
        //please build code here as long as you can
    }



    //User menu controller
    public void menuUserControl(Connection connection){

        List<Products> arr = new ArrayList<>();
        Products products = new Products();
        //ManagmentTool mt = new ManagmentTool();
        ManageText mt = new ManageText();
        Validation val = new Validation();
        Scanner sc = new Scanner(System.in);

        mt.logo();

        RestoreProduck(connection,products,arr);// restore auto -------

        mt.loading(arr);//lorder follow number of array element

        int defaultPage =1;
        int defaultRow =4;
        int autoProduct_id;
        int sizeArray;

        if (arr.size() == 0){
            autoProduct_id = 0;
        }else {
            autoProduct_id = arr.get(arr.size()-1).getId();
        }

        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.left);
        Table t = new Table(6, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.SURROUND);
        Table t1 = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.SURROUND);
        Table t2 = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.SURROUND);
        Table t3 = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.SURROUND);

        t.addCell("  di)splay  ",numberStyle);
        t.addCell("  w)rite  ",numberStyle);
        t.addCell("  r)ead  ",numberStyle);
        t.addCell("  u)pdate  ",numberStyle);
        t.addCell("  d)elete  ",numberStyle);
        t.addCell("  f)irst  ",numberStyle);
        t.addCell("  n)ext  ",numberStyle);
        t.addCell("  p)revious  ",numberStyle);
        t.addCell("  l)ast  ",numberStyle);
        t.addCell("  g)oto  ",numberStyle);
        t.addCell("  sr)setrow  ",numberStyle);
        t.addCell("  s)earch  ",numberStyle);
        t.addCell("  e)xit  ",numberStyle);
        System.out.println(t.render());

        t1.addCell("--- ! No Data.! ---",numberStyle);
        t2.addCell("----------- Please Inform Your Product data -----------");
        t3.addCell("----------- Update's Inform Your Product data -----------");

        System.out.print("--> Choose Option: ");
        String option = sc.nextLine().toLowerCase();
        boolean check = val.checkStr(option);
        while (!check){
            System.out.print("--> Enter only chracter: ");
            option = sc.nextLine().toLowerCase();
            check = val.checkStr(option);
        }

        while (option != "e"){
            switch (option){

                case "di"://display in table ----------

                    if (arr.size()==0){

                        System.out.println(t1.render());

                    }else {

                        displayPage(arr,defaultPage,defaultRow);//process--->

                    }

                    System.out.println(t.render());

                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){

                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);

                    }

                    break;

                case "w"://insert data to in table ----------

                    System.out.println(t2.render());
                    if(arr.size() == 0){
                        autoProduct_id = 0;
                    }else {
                        autoProduct_id = arr.get(arr.size()-1).getId();
                    }
                    autoProduct_id = autoProduct_id+1;

                    int newId = writeDataToArray(arr,autoProduct_id,products,connection);//process--->
                    autoProduct_id = newId;

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }

                    break;

                case "r"://read data by id in table ----------

                    System.out.print("-->> Enter Product_id: ");
                    String product_id = sc.nextLine();
                    check = val.checkNum(product_id);
                    while (!check){
                        System.out.print("-->> Enter Product_id again: ");
                        product_id = sc.nextLine();
                        check = val.checkNum(product_id);
                    }
                    int intid = Integer.parseInt(product_id);

                    readDataById(arr,intid);//process--->

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }

                    break;

                case "u"://update data to table ----------
                    System.out.println(t3.render());

                    System.out.print("-->>Enter Product_id: ");
                    product_id = sc.nextLine();
                    check = val.checkNum(product_id);
                    while (!check){
                        System.out.print("-->>Enter Product_id again: ");
                        product_id = sc.nextLine();
                        check = val.checkNum(product_id);
                    }
                    intid = Integer.parseInt(product_id);

                    updateDateToArrayList(arr,intid,products,connection);//process--->

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }

                    break;

                case "d"://delete data form table ----------

                    System.out.print("-->>Enter Product_id: ");
                    product_id = sc.nextLine();

                    check = val.checkNum(product_id);
                    while (!check){
                        System.out.print("-->>Enter Product_id again: ");
                        product_id = sc.nextLine();
                        check = val.checkNum(product_id);
                    }
                    intid = Integer.parseInt(product_id);

                    int i = removeDataFromArrayList(arr,intid,connection);//process--->
                    System.out.println(i);

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }

                    break;

                case "f"://move to first page in table ----------

                    defaultPage = 1;
                    defaultPage = displayPage(arr,defaultPage,defaultRow);

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }

                    break;

                case "p"://move to Previous page in table ----------

                    defaultPage = defaultPage-1;

                    if (defaultPage<0){

                        defaultPage = arr.size()/defaultRow;

                    }

                    defaultPage = displayPage(arr,defaultPage,defaultRow);// process ---->

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }

                    break;

                case "n"://move to Next page in table ----------

                    defaultPage = defaultPage+1;

                    if ( defaultPage > (arr.size()/defaultRow) ){

                        defaultPage = 1;

                    }

                    defaultPage = displayPage(arr,defaultPage,defaultRow);// process ---->

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }

                    break;

                case "l"://move to Last Page in Table ----------

                    sizeArray = arr.size()/defaultRow;
                    if ((arr.size()%defaultRow)== 0){
                        defaultPage = sizeArray;
                    }else {
                        defaultPage = sizeArray + 1;
                    }

                    defaultPage = displayPage(arr,defaultPage,defaultRow);

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }

                    break;

                case "g":
                    System.out.println("-------- Which Page You want To Display --------");

                    System.out.print("Go to page: ");
                    String goToPage = sc.nextLine();
                    check = val.checkNum(goToPage);

                    while (!check){
                        System.out.print("Go to page: ");
                        goToPage = sc.nextLine();
                        check = val.checkNum(goToPage);
                    }
                    int intgoToPage = Integer.parseInt(goToPage);

                    defaultPage = displayPage(arr,intgoToPage,defaultRow);

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }

                    break;

                case "sr":

                    System.out.println("-------- How Many Row That You want To Display --------");

                    System.out.print("Enter Number Row: ");
                    String strRow = sc.nextLine();
                    check = val.checkNum(strRow);
                    while (!check){
                        System.out.print("Enter Number Row: ");
                        strRow = sc.nextLine();
                        check = val.checkNum(strRow);
                    }
                    int intRow = Integer.parseInt(strRow);

                    displayPage(arr,defaultPage,intRow);//process--->
                    defaultRow = intRow;

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }

                    break;

                case "s":



                    break;

                case "e":
                    option = "e";
                    break;

                default:
                    System.out.println("--- ! You use wrong option. ! ---");
                    option = "e";

            }//end switch -------
        }//end while----
        System.out.println("----- Thanks You.! ----- ");

    }

    //end arrayMethod---------------------------------------------------------------------------------------------------
} //end ManagementTool Class----------
