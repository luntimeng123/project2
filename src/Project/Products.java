package Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Products {

    //class member declaration
    private int id;
    private String pname;
    private float unit_price;
    private int qty;
    private String date;

    //generate getter
    public int getId() {
        return id;
    }

    public String getPname() {
        return pname;
    }

    public float getUnit_price() {
        return unit_price;
    }

    public int getQty() {
        return qty;
    }

    public String getDate() {
        return date;
    }

    //generate setter
    public void setId(int id) {
        this.id = id;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public void setUnit_price(float unit_price) {
        this.unit_price = unit_price;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setDate(String date) {
        this.date = date;
    }

    //constructer with no parameter
    public Products(){ }

    //contructor with parameter

    public Products(int id, String pname, float unit_price, int qty, String date) {
        this.id = id;
        this.pname = pname;
        this.unit_price = unit_price;
        this.qty = qty;
        this.date = date;
    }


    //generate toString()

    @Override
    public String toString() {
        return "Id: "+this.id+" / "+"PName: "+this.pname+" / "+"Unit_Price: "+this.unit_price+" / "+"Qty: "+this.qty+" / "+"Date: "+this.date+"\n";
    }


    //end class---------------------------------------------------------------------------------------------------------
}
