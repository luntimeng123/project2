package Project;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

    public boolean checkStr(String str){
        String str1 = "[a-zA-Z]*";
        Pattern pat = Pattern.compile(str1,Pattern.CASE_INSENSITIVE);
        Matcher match = pat.matcher(str);
        Boolean check = match.matches();
        return check;
    }

    public boolean checkNum(String num){
        String Number = "\\d*";
        Pattern mypat = Pattern.compile(Number,Pattern.CASE_INSENSITIVE);
        Matcher mymatch = mypat.matcher(num);
        boolean check = mymatch.matches();
        return check;
    }

}
